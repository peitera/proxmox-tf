variable "prox_api_url" {
  type        = string
  description = "url:port of the proxmox server"
  default     = "pve.lab:8006"
}
variable "prox_user" {
  type        = string
  description = "user and method to login with"
  default     = "tf@pve"
}

variable "prox_password" {
  type        = string
  description = "password for proxmox host"
}

variable "vm_name" {
  type        = string
  description = "name of VM to create"
}

variable "vm_clone" {
  type        = string
  description = "template to clone from"
  default     = "ubuntu-18-CLONE"
}

variable "vm_cores" {
  type        = number
  description = "number of CPU cores to utilise"
  default     = 2
}

variable "vm_mem" {
  type        = number
  description = "memory to allocate in Megabytes"
  default     = 1024
}

variable "vm_disk_size" {
  type        = number
  description = "disc size to allocate, in Gigabytes"
  default     = 10
}

variable "vm_disk_drive" {
  type        = string
  description = "storage drive to store disk on"
  default     = "local-lvm"
}